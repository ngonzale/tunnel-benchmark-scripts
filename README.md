# Tunnel Driver Benchmark Scripts

 - [./iperf3-client.py](`iperf3-client.py`): Call `iperf3` on the client and
     pipe the output through this
 - [./iperf3-server.py](`iperf3-server.py`): Call `iperf3` on the server and pipe
     the output through this
 - [./ping.py](`ping.py`): Call `ping` and pipe the output through this

## Plotters

 - [./plot_iperf3.py](`plot_iperf3.py`): Fill in the `files` table with client
     and server data files, and a name, and run it
 - [./plot_ping.py](`plot_ping.py`): Fill in the `files` table with ping data
     and a name and run it
