#!/usr/bin/python3

"""Iperf3 Analysis"""

import sys
import statistics
from time import time

def compute(val: str, unit: str) -> int:
    """Compute the correct value for the output"""
    val_decoded = float(val)
    return int({
        "bits/sec": lambda x: x,
        "Kbits/sec": lambda x: 1000*x,
        "Mbits/sec": lambda x: 1000*1000*x,
        "Gbits/sec": lambda x: 1000*1000*1000*x,
        }.get(unit, lambda x: -1)(val_decoded))


def main():
    """Main function"""
    print("Analysis called")
    data = []
    ip_addr = ""
    buff = ""
    keep_going = True
    while keep_going and (content := sys.stdin.read(16)):
        buff += content
        #print(f"+{len(content)} bytes: {buff.encode('utf-8')}", flush=True)
        while "\n" in buff:
            pos = buff.find('\n')
            line = buff[:pos]
            buff = buff[pos+1:]
            line = line.strip()
            if "local" in line:
                # It's the line that gives the IPv4 address
                target = line.split("connected to")[1].strip().split(' ')[0]
                ip_addr = target
            if "- - - - -" in line:
                # It's the dash line before the final stats, break out
                keep_going = False
                break
            if "Bytes" not in line:
                continue

            # Begin analysis
            splits = line.replace("  ", " ")
            while "  " in splits:
                splits = splits.replace("  ", " ")
            splits = splits.split("Bytes")[1].strip().split(' ')

            value, unit, retr = splits[0:3]
            if retr == "receiver":
                # This is the last line with the global stats, ignore it
                continue

            # data_bitrate.append(compute(value, unit))
            data.append(int(retr))
            print(f"[{ip_addr}] B={value} {unit} w/ {retr} ({len(data)})   ",
                  flush=True, end='\r')

    print()
    print("\nRetry statistics:\n-------------------")
    print(data)
    print(f"AVG={statistics.mean(data)}\tMED={statistics.median(data)}")
    print(f"STD={statistics.stdev(data)}\tMIN={min(data)}\tMAX={max(data)}")
    print(f"QTL={statistics.quantiles(data)}")

    filename = f"benchmark-iperf3-{int(time())}-client.bch"
    with open(filename, "w", encoding="utf-8") as fptr:
        fptr.write(ip_addr + ":" + str(data) + "\n")
    print(f"Saved to {filename}")

if __name__ == "__main__":
    main()
