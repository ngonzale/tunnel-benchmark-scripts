#!/usr/bin/python3

"""Ping data plotter"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def get_data(files):
    """Get data from files"""
    pings = []
    labels = []
    for path, name in files:
        with open(path, "r", encoding="utf-8") as fptr:
            data = list(map(float, fptr.read().split(":")[1].strip()[1:-1].split(", ")))
        pings.append(np.array(data))
        labels.append(name)

    return pings, labels


def main():
    """Main function"""
    files = [
        ("/home/shared/dzemael/benchmark-ping-1684847979.bch", "WireGuard"),
        ("/home/shared/dzemael/benchmark-ping-1684848990.bch", "Rust Tunnels"),
        ("/home/shared/dzemael/benchmark-ping-1684850001.bch", "C Tunnels"),
        ("/home/shared/dzemael/benchmark-ping-1684851012.bch", "Bridge"),
    ]

    font = { 'size': '16' }
    matplotlib.rc('font', **font)

    arr, labels = get_data(files)

    _figure, axis = plt.subplots()
    axis.set_title(f"Latency (ms) (n={len(arr[0])})")
    axis.boxplot(arr)
    plt.xticks(range(1, len(arr) + 1), labels)

    plt.savefig("/tmp/plot-ping.png", bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    main()
