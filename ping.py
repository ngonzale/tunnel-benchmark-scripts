#!/usr/bin/python3

"""Ping Analysis"""

import sys
import re
import statistics
from time import time

def main():
    """Main function"""
    print("Analysis called")
    pat = re.compile(r"^(\d+) bytes from ((\d+.){3}\d+): " +
                     r"icmp_seq=(\d+) ttl=(\d+) time=([0-9.]+) ms$")
    data = []
    ip_addr = ""
    buff = ""
    while (content := sys.stdin.read(64)):
        buff += content
        if not "\n" in buff:
            print("c")
            continue
        while "\n" in buff:
            pos = buff.find('\n')
            line = buff[:pos]
            buff = buff[pos+1:]
            line = line.strip()
            match = pat.fullmatch(line)
            if match is None:
                continue
            groups = match.groups()
            ping_ms = float(groups[-1])
            ip_addr = groups[1]
            data.append(ping_ms)
            print(f"[{ip_addr}] T={ping_ms} ({len(data)}) ", flush=True, end='\r')

    print()
    print(data)
    print(f"AVG={statistics.mean(data)}\tMED={statistics.median(data)}")
    print(f"STD={statistics.stdev(data)}\tMIN={min(data)}\tMAX={max(data)}")
    print(f"QTL={statistics.quantiles(data)}")

    filename = f"benchmark-ping-{int(time())}.bch"
    with open(filename, "w", encoding="utf-8") as fptr:
        fptr.write(ip_addr + ":" + str(data) + "\n")
    print(f"Saved to {filename}")

if __name__ == "__main__":
    main()
