#!/usr/bin/python3

"""Iperf3 data plotter"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter

def get_data(files):
    """Get data from files"""
    pings = []
    labels = []
    for path, name, _ in files:
        with open(path, "r", encoding="utf-8") as fptr:
            data_fields = fptr.read().split(":")
            data_bitrate = list(map(float, data_fields[1].strip()[1:-1].split(", ")))
            data_retr = list(map(float, data_fields[2].strip()[1:-1].split(", ")))
        # data.sort()
        pings.append([np.array(data_bitrate), np.array(data_retr)])
        labels.append(name)

    return pings, labels

def main():
    """Main function"""
    files = [
        ("/home/shared/dzemael/report-benchmarks/benchmark-iperf3-1685625842.bch", "Bridge", [0, 0]),
        ("/home/shared/dzemael/report-benchmarks/benchmark-iperf3-1685626845.bch", "Rust Tunnels", [1, 0]),
        ("/home/shared/dzemael/report-benchmarks/benchmark-iperf3-1685624838.bch", "C Tunnels", [1, 1]),
        ("/home/shared/dzemael/report-benchmarks/benchmark-iperf3-1685627849.bch", "WireGuard", [0, 1]),
    ]

    font = {
            'size': 16
            }
    matplotlib.rc('font', **font)

    arr, labels = get_data(files)
    point_count = len(arr[0])
    used_axes = []

    colors = ["blue", "darkblue", "lime", "darkgreen", "orange", "saddlebrown",
              "red", "darkred"]
    tp_color = "tab:purple"
    rt_color = "tab:brown"

    figure, axis_big = plt.subplots(2, 2, sharex='col', sharey='row')
    engformat = EngFormatter(sep="\n\N{THIN SPACE}", unit="bps")
    for (i, file) in enumerate(files):
        axis = axis_big[*file[2]]
        axis_two = axis.twinx()
        axis.set_title(f"{file[1]} (n={len(arr[i][0])})")
        #axis.boxplot(arr)
        # print(arr[i])
        axis.scatter(np.arange(0, len(arr[i][0])), arr[i][0], marker='x',
                     label=f"{file[1]} (bitrate)", color=colors[2*i], s=15)
        axis_two.scatter(np.arange(0, len(arr[i][1])), arr[i][1], marker=(3,0),
                         label=f"{file[1]} (retrans.)", color=colors[2*i+1],
                         s=15)

        # axis.scatter(x, y)
        #point_count, histtype='step', cumulative=False, label=file[1])
        # cnt, edges = np.histogram(arr[i], bins=point_count)
        # axis.step(arr[i], cnt.cumsum(), label=files[i][1])
        if file[2][1] == 0:
            axis.set_ylabel("Troughput", color=tp_color)
        axis.yaxis.set_major_formatter(engformat)
        if file[2][1] == 1:
            axis_two.set_ylabel("TCP Retransmission Count (per second)",
                            color=rt_color)
        if file[2][0] == 1:
            axis.set_xlabel("Time since test began (s)")
        #axis.set_ylabel("Distribution")
        axis.grid(True)
        axis.tick_params(axis="y", labelcolor=tp_color)
        axis_two.tick_params(axis="y", labelcolor=rt_color)
        #plt.yscale("log")

        used_axes.append(axis)
        used_axes.append(axis_two)

    figure.legend(loc='lower left')
    for axis in used_axes:
        axis.set_ylim(ymin=0)


    # plt.suptitle("TCP Throughput Performance")
    plt.subplots_adjust(left=0.24, right=0.95, top=0.95, bottom=0.06)
    # plt.subplots_adjust(left=0.21, right=0.95, top=0.95, bottom=0.06)
    plt.savefig("/tmp/plot-iperf3.png", bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    main()
